{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module NotenspiegelParser (
    Module(..)
) where

import Data.Yaml
import Data.Ratio (Ratio)
import Data.Aeson (FromJSONKey, fromJSONKey, FromJSONKeyFunction(FromJSONKeyTextParser))
import qualified Data.Map.Strict as M
import Numeric (readSigned, readFloat)
import qualified Data.Text as T

instance FromJSONKey (Ratio Integer) where
    fromJSONKey = FromJSONKeyTextParser $ readRational . T.unpack

readRational :: MonadFail m => String -> m Rational
readRational s = case readSigned readFloat $ s of
        (f, _):_ -> return f
        _ -> fail $ "Can't parse rational"

data Module a = Module {
    moduleName :: String,
    moduleEcts :: Int,
    gradesKnown :: M.Map String a,
    gradesDist :: M.Map a Int
} deriving (Show, Eq)

instance (FromJSON a, FromJSONKey a, Ord a) => FromJSON (Module a) where
    parseJSON = withObject "Module" $ \o ->
        Module
        <$> o .: "name"
        <*> o .: "ects"
        <*> o .: "known_grades"
        <*> o .: "distribution"
