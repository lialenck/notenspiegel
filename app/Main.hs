{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main (main) where

import Control.Monad (forM_)
import Data.Yaml (decodeFileThrow)
import NotenspiegelParser
import qualified Data.Map.Strict as M

modulAverage :: Fractional a => Module a -> a
modulAverage m = let grades = M.toList $ gradesDist m
                     gradeSum = sum $ map (\(a, b) -> a * fromIntegral b) grades
                     participants = sum $ map snd grades
    in gradeSum / fromIntegral participants

modulMedian :: Num a => Module a -> a
modulMedian m = let grades = M.toList $ gradesDist m
                    participants = sum $ map snd grades
                    go off ((g, n):more) = if off + n > participants `div` 2
                                           then g
                                           else go (off + n) more
    in go 0 grades

main :: IO ()
main = do
    modules <- decodeFileThrow "notenspiegel.yaml" :: IO [Module Double]
    mapM_ print modules
    forM_ modules $ \m -> do
        putStrLn $ moduleName m ++ ": " ++ show (modulAverage m)
